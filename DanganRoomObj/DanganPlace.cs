﻿using System;
using System.IO;
using System.Collections.Generic;

namespace DanganRoomObj
{
	public enum DanganObjectType
	{
		CHAR_STATIC = 1,
		SPAWN = 3,
		INTERACTABLE = 8
	}

	public struct DanganRoomObject
	{
		public DanganObjectType ObjectType;

		public int ID;
		public int ModelID;

		public float X;
		public float Y;
		public float Z;

		public float Width;
		public float Height;

		public float Rotation;

		public int Unknown2;
	}
	public class DanganPlace
	{
		public List<DanganRoomObject> Objects = new List<DanganRoomObject>();

		public static DanganPlace ReadFromFile( string filepath )
		{
			using ( BinaryReader br = new BinaryReader( File.Open( filepath, FileMode.Open ) ) )
			{
				DanganPlace readPlace = new DanganPlace();

				int objCount = br.ReadInt32();


				for ( int i = 0; i<objCount; i++ )
				{
					int offs = br.ReadInt32();
					long oldPos = br.BaseStream.Position;

					br.BaseStream.Seek( (long)offs, SeekOrigin.Begin );

					DanganRoomObject newObj = new DanganRoomObject();

					newObj.ObjectType = (DanganObjectType)br.ReadInt32();

					newObj.ID = br.ReadInt32();
					newObj.ModelID = br.ReadInt32();

					newObj.X = br.ReadSingle();
					newObj.Y = br.ReadSingle();
					newObj.Z = br.ReadSingle();

					newObj.Width = br.ReadSingle();
					newObj.Height = br.ReadSingle();

					newObj.Rotation = br.ReadSingle();

					newObj.Unknown2 = br.ReadInt32();

					readPlace.Objects.Add( newObj );

					br.BaseStream.Seek( oldPos, SeekOrigin.Begin );
				}

				return readPlace;
			}
		}

		public void WriteToFile( string filename )
		{
			using ( BinaryWriter bw = new BinaryWriter( File.Open( filename, FileMode.Create ) ) )
			{
				List<int> offsetList = new List<int>();

				//We explicitly cast all of these values so
				//the writer doesn't try to be a smart-ass
				//and convert them.

				bw.Write( (int)Objects.Count );
				long offsetsStart = bw.BaseStream.Position;
				for ( int i = 0; i<Objects.Count; i++ )
				{
					//Fill out dummy offsets
					bw.Write( (int)0 );
				}

				foreach ( DanganRoomObject obj in Objects )
				{
					offsetList.Add( (int)bw.BaseStream.Position );
					bw.Write( (int)obj.ObjectType );

					bw.Write( (int)obj.ID );
					bw.Write( (int)obj.ModelID );

					bw.Write( (float)obj.X );
					bw.Write( (float)obj.Y );
					bw.Write( (float)obj.Z );

					bw.Write( (float)obj.Width );
					bw.Write( (float)obj.Height );

					bw.Write( (float)obj.Rotation );

					bw.Write( (int)obj.Unknown2 );
				}

				bw.Seek( (int)offsetsStart, SeekOrigin.Begin );

				foreach ( int offset in offsetList )
				{
					bw.Write( (int)offset );
				}
			}
		}
	}
}
